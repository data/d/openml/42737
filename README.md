# OpenML dataset: fps-in-video-games

https://www.openml.org/d/42737

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Sven Peeters  
**Source**: userbenchmark.com, fpsbenchmark.com, techpowerup.com - June 2020  
**Please cite**: TBA

# Achieved Frames per Second (FPS) in video games

This dataset contains FPS measurement of video games executed on computers. Each row of the dataset describes the outcome of FPS measurement (outcome is attribute FPS) for a video game executed on a computer. A computer is characterized by the CPU and the GPU. For both the name is resolved to technical specifications (features starting with Cpu and Gpu). The technical specification of CPU and GPU are technical specification that describe the factory state of the respective component. The game is characterized by the name, the displayed resolution, and the quality setting that was adjusted during the measurement (features starting with Game). In the following there is a short descriptions of the data sources and a description for each feature in the dataset.

## Source of Data

The measurements are gathered from the webpages userbenchmark.com and fpsbenchmark.com. The source of each measurement is indicated by the attribute dataset wich takes the value userbenchmark or fpsbenchmark. For userbenchmark the FPS values are extracted from the published histograms. Hence, the FPS values are binned to bins with width 10 starting at 10 and there are multiple measurements for the same computer and software configuration (CpuName, GpuName, GameName, GameSetting, GameResolution, Dataset). The technical specifications are gathered from the webpage techpowerup.com. All FPS measurements and technical specifications were crawled in June 2020.

## Description of the technical features

### CPU (Central Processing Unit)

* CpuNumberOfCores: number of physical cores 
* CpuNumberOfThreads: number of threads
* CpuBaseClock: base clock in Mhz
* CpuCacheL1: total size of level 1 cache in kB 
* CpuCacheL2: total size of level 2 cache in kB 
* CpuCacheL3: total size of level 3 cache in MB
* CpuDieSize: physical size of the die in square meter
* CpuFrequency: frequency in Mhz
* CpuMultiplier: multiplier of Cpu
* CpuMultiplierUnlocked: 0=multiplier locked, 1=multiplier unlocked 
* CpuProcessSize: used process size in nanometer 
* CpuSMPNumberOfCpus: number of symmetric multiprocessors
* CpuTDP: thermal design power in watt
* CpuTransistors: count of transistors in million
* CpuTurbo Clock: turbo clock in Mhz

### GPU (Graphics Processing Unit)

* GpuBandwidth: bandwidth in MB/s 
* GpuBaseClock: base clock in MHz
* GpuBoostClock: boost clock in MHz 
* GpuComputeUnits: number of computing units
* GpuDieSize: physical size of die in square meter 
* GpuNumberOfExecutionUnits: number of execution units 
* GpuFP32Performance: theoretical Float 32 performance in MFLOP/s 
* GpuMemoryBus: width of memory bus in bits
* GpuMemorySize: size of memory in MB 
* GpuPixelRate: theoretical pixel rate in MPixel/s 
* GpuProcessSize:  used process size in nanometer 
* GpuNumberOfROPs: number of render output units
* GpuNumberOfShadingUnits: number of shading units
* GpuNumberOfTMUs: number of texture mapping units 
* GpuTextureRate: theoretical texture rate in KTexel/s
* GpuTransistors: number of transistors in million
* GpuArchitecture: architecture code
* GpuMemoryType: memory type
* GpuBusInterface: bus interface
* GpuOpenCL: version of OpenCL
* GpuShaderModel: version of shader model
* GpuVulkan: version of Vulkan 
* GpuOpenGL: version of OpenGL

### Game

* GameName: name 
* GameResolution: resolution 
* GameSetting: quality setting 

### Dataset indicator and Target

* Dataset: indicator for the source of measurement 
* FPS: FPS value

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42737) of an [OpenML dataset](https://www.openml.org/d/42737). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42737/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42737/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42737/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

